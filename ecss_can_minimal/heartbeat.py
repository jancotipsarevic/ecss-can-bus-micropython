import pyb

from .message import Message
from .constants import TIMER_HEARTBEAT_PRODUCER, TIMER_HEARTBEAT_CONSUMER
from .constants import COB_ID_HEARTBEAT
from .constants import LED_HEARTBEAT_PRODUCER, LED_HEARTBEAT_CONSUMER
from .constants import OD_INDEX_PRODUCER_HEARTBEAT_TIME
from .constants import OD_INDEX_CONSUMER_HEARTBEAT_TIME
from .constants import OD_INDEX_REDUNDANCY_MANAGEMENT, OD_SUBINDEX_T_TOGGLE
from .constants import OD_SUBINDEX_C_TOGGLE, OD_SUBINDEX_N_TOGGLE
from .constants import OD_SUBINDEX_B_DEFAULT


class HeartbeatProducer:
    """Transmit a HB message periodically."""
    def __init__(self, node):
        self.node = node
        self.message = Message(can_id=COB_ID_HEARTBEAT + self.node.id,
                               data=self.node.state)
        self.timer = pyb.Timer(TIMER_HEARTBEAT_PRODUCER)
        self.period = None
        self.running = False

    def send(self):
        self.message.data = self.node.state
        self.node.network.send_message(self.message)

    def callback(self, timer):
        if LED_HEARTBEAT_PRODUCER:
            pyb.LED(LED_HEARTBEAT_PRODUCER).toggle()
        self.send()

    def start(self):
        """Start periodic transmission."""
        tmp = self.node.od[OD_INDEX_PRODUCER_HEARTBEAT_TIME]
        if tmp is not None:
            self.period = tmp.value / 1000

        if not self.period:
            raise ValueError("No valid transmission period.")

        if self.running:
            self.stop()

        self.timer.init(freq=1/self.period)
        self.timer.callback(self.callback)
        self.running = True

    def stop(self):
        """Stop periodic transmission."""
        if self.running:
            self.timer.deinit()
            self.timer.callback(None)
            self.running = False


class HeartbeatConsumer:
    """Listen for the periodic HB message from a node and trigger action
    if heartbeat is missing."""
    def __init__(self, node):
        self.node = node
        self.timer = pyb.Timer(TIMER_HEARTBEAT_CONSUMER)
        self.period = None
        self.running = False
        self.missed_heartbeats = 0

    def callback(self, timer):
        print("info: heartbeat missed")
        self.missed_heartbeats += 1
        if (self.missed_heartbeats >
                self.node.od[OD_INDEX_REDUNDANCY_MANAGEMENT][OD_SUBINDEX_T_TOGGLE].value):
            # max number of allowed missing heartbeats (t_toggle) reached
            # check if a bus switch is still allowed
            if (self.node.od[OD_INDEX_REDUNDANCY_MANAGEMENT][OD_SUBINDEX_C_TOGGLE].value <
                    self.node.od[OD_INDEX_REDUNDANCY_MANAGEMENT][OD_SUBINDEX_N_TOGGLE].value):
                print("info: switching bus")
                self.node.network.switch_bus()
                self.node.od[OD_INDEX_REDUNDANCY_MANAGEMENT][OD_SUBINDEX_C_TOGGLE].value += 1
                self.missed_heartbeats = 0
            else:
                print("warning: bus switch limit reached")

    def start(self):
        """Start listening for heartbeat."""
        tmp = self.node.od[OD_INDEX_CONSUMER_HEARTBEAT_TIME]
        if tmp is not None:
            self.period = tmp.value / 1000

        if not self.period:
            raise ValueError("No valid transmission period.")

        if self.running:
            self.stop()

        self.timer.init(freq=1/self.period)
        self.timer.callback(self.callback)
        self.running = True

    def stop(self):
        """Stop listening for heartbeat."""
        if self.running:
            self.timer.deinit()
            self.timer.callback(None)
            self.running = False

    def received(self):
        """Reset heartbeat timout counter ."""
        if self.running:
            active_bus = self.node.network.get_active_bus()
            self.node.od[OD_INDEX_REDUNDANCY_MANAGEMENT][OD_SUBINDEX_B_DEFAULT].value = \
                active_bus.channel
            if LED_HEARTBEAT_CONSUMER:
                pyb.LED(LED_HEARTBEAT_CONSUMER).toggle()
            self.timer.counter(0)
            self.missed_heartbeats = 0
