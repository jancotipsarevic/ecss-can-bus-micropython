from ecss_can_minimal.objectdictionary import ObjectDictionary
from ecss_can_minimal.objectdictionary import Variable, Record
from ecss_can_minimal.constants import *


SLAVE_NODE_ID = 2


class SlaveNodeObjectDictionary(ObjectDictionary):
    def __init__(self, node_id=None):
        super().__init__(node_id)

        # define SYNC object
        super().add_object(OD_INDEX_COMMUNICATION_CYCLE_PERIOD,
                           Variable(1000000, "Communic. cycle period (usec)"))

        # define producer and consumer heartbeat configuration
        super().add_object(OD_INDEX_PRODUCER_HEARTBEAT_TIME,
                           Variable(250, "Producer heartbeat time (msec)"))
        super().add_object(OD_INDEX_CONSUMER_HEARTBEAT_TIME,
                           Variable(500, "Consumer heartbeat time (msec)"))

        # redundancy management
        self.bdefault = Variable(1, "Bdefault")
        rec = Record("Redundancy management")
        rec.add_member(0, Variable(4))  # 4 subindeces
        rec.add_member(1, Variable(self.bdefault, "Bdefault"))
        rec.add_member(2, Variable(5, "Ttoggle"))
        rec.add_member(3, Variable(10, "Ntoggle"))
        rec.add_member(4, Variable(0, "Ctoggle"))
        super().add_object(OD_INDEX_REDUNDANCY_MANAGEMENT, rec)

        # TPDO3 for TM_REQUEST reply
        rec = Record("TPDO3 parameter")
        rec.add_member(0, Variable(2))  # 2 subindeces
        rec.add_member(1, Variable(COB_ID_TPDO_3))
        rec.add_member(2, Variable(254, "transmission type"))
        super().add_object(OD_INDEX_TPDO3, rec)

        self.tm_req_code_mapping = Variable(
            "{:04x}".format(OD_INDEX_TM_REQ_CODE) + "00" + "10",
            "TM request code mapping")
        self.tm_mapping = Variable(None, "TM mapping")
        rec = Record("TPDO3 mapping")
        rec.add_member(0, Variable(2))  # 2 subindeces
        rec.add_member(1, self.tm_req_code_mapping)
        rec.add_member(2, self.tm_mapping)
        super().add_object(OD_INDEX_TPDO3_MAPPING, rec)

        # TM REQ code
        self.tm_req_code = Variable(None)
        super().add_object(OD_INDEX_TM_REQ_CODE, self.tm_req_code)

        # TM dummy data for TM REQ 0x2211
        rec = Record()
        self.dummy_sensor_data = Variable(0x998877665544)
        rec.add_member(0x11, self.dummy_sensor_data)
        super().add_object(OD_INDEX_TM + 0x22, rec)
